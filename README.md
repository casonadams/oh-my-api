# oh-my-api

Oh my this darn API

## getting started

It is recommended to use [direnv](https://direnv.net/)

### setup environment vars

- `cp envrc-sample .envrc`
- update `.envrc` to use correct values
- `direnv allow` or `source .envrc`

### spin up environment

- build image `docker-compose build dev`
- spin up container `docker-compose up -d dev`
- connect to container shell `docker exec -it oh-my-api bash`

### running tests

- run tests `./bin/test.sh`

### spin down environment

- exit container shell `exit`
- sping down container `docker-compose down`
