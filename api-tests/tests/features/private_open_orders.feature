Feature: Correctly handles the 2 factor authentication requirements; Requests and reports all open orders on the account. Report the results of the scan.

  Scenario: POST /private
    Given API-Key: OMA_API_KEY with otp_secret: OMA_API_TOTP_SECRET
    When a POST request is made to /0/private/OpenOrders
    Then the response is valid
