Feature: Connects to the public API; retrieves and reports the information for the XBT/USD trading pair, validate response

  Scenario: GET public/Ticker?pair
    Given internet access
    When a GET request is made to /0/public/Ticker?pair=param
      | param  |
      | XBTUSD |
    Then the response is valid
