Feature: Connects to the public API and retrieves the server time, validate response format

  Scenario: GET /public/Time
    Given internet access
    When a GET request is made to /0/public/Time
    Then the response rfc1123 time is valid
