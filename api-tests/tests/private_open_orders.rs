use api::private::OpenOrdersResponse;
use cucumber::{given, then, when, WorldInit};

#[given(regex = r"^API-Key: ([A-Z_]+) with otp_secret: ([A-Z_]+)$")]
async fn is_authorized(r: &mut OpenOrdersResponse, api_key_name: String, totp_secret_name: String) {
  r.api_key_envvar = Some(api_key_name);
  r.totp_secret_envvar = Some(totp_secret_name);
}

#[when(regex = r"^a POST request is made to ([/\d\w]+)$")]
async fn get_request(r: &mut OpenOrdersResponse, urlpath: String) {
  let response = r.get(urlpath, true).await.expect("response");
  r.result = response.result;
  r.error = response.error;
}

#[then("the response is valid")]
async fn is_valid(r: &mut OpenOrdersResponse) {
  let empty: &usize = &0;
  assert_eq!(&r.error.len(), empty);
}

#[tokio::main]
async fn main() {
  OpenOrdersResponse::run("tests/features/private_open_orders.feature").await;
}
