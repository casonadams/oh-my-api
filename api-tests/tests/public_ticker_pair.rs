use api::public::TickerPairResponse;
use cucumber::{gherkin::Step, given, then, when, WorldInit};

#[given(expr = "internet access")]
async fn internet_access(_r: &mut TickerPairResponse) {
  let response = reqwest::get("https://ifconfig.io")
    .await
    .expect("a response")
    .text()
    .await;

  assert!(response.is_ok())
}

#[when(regex = r"^a GET request is made to ([/\d\w]+\?pair=)\w+$")]
async fn get_request(r: &mut TickerPairResponse, step: &Step, public_ticker_pair_uri: String) {
  if let Some(table) = step.table.as_ref() {
    // NOTE: skip header
    for row in table.rows.iter().skip(1) {
      let response = TickerPairResponse::get(public_ticker_pair_uri.clone(), row.concat())
        .await
        .expect("response to be in format TickerPairResponse");
      if !response.error.is_empty() {
        let e = &response.error.concat();
        r.error.push(e.to_string());
      }
    }
  }
}

// This actually isn't the case the response will be valid, just checking for
// unexpected errors here
#[then("the response is valid")]
async fn is_valid(r: &mut TickerPairResponse) {
  assert!(r.error.is_empty());
}

#[tokio::main]
async fn main() {
  TickerPairResponse::run("tests/features/public_ticker_pair.feature").await;
}
