use api::public::TimeResponse;
use chrono::prelude::{DateTime, NaiveDateTime, Utc};
use cucumber::{given, then, when, WorldInit};

#[given(expr = "internet access")]
async fn internet_access(_r: &mut TimeResponse) {
  let response = reqwest::get("https://ifconfig.io")
    .await
    .expect("a response")
    .text()
    .await;

  assert!(response.is_ok())
}

#[when(regex = r"^a GET request is made to ([/\d\w]+)$")]
async fn get_request(r: &mut TimeResponse, public_time_uri: String) {
  let response = TimeResponse::get(public_time_uri).await;
  assert!(response.as_ref().is_ok());

  let data = response.expect("response to be in format NetResponse");
  r.result = data.result;
  r.error = data.error;
}

#[then("the response rfc1123 time is valid")]
async fn is_valid(r: &mut TimeResponse) {
  let naive = NaiveDateTime::from_timestamp(r.result.unixtime, 0);
  let datetime: DateTime<Utc> = DateTime::from_utc(naive, Utc);
  let rfc1123 = datetime.format("%a, %d %b %y %H:%M:%S %z").to_string();
  assert!(r.result.rfc1123 == rfc1123);
}

#[tokio::main]
async fn main() {
  TimeResponse::run("tests/features/public_time.feature").await;
}
