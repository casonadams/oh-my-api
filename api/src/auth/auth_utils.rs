use anyhow::{anyhow, Result};
use chrono::prelude::*;
use data_encoding::BASE64;
use google_authenticator::GoogleAuthenticator;
use hmac::{Hmac, Mac};
use sha2::{Digest, Sha256, Sha512};
use std::collections::HashMap;

pub type HmacSha512 = Hmac<Sha512>;

pub fn generate_otp_code(totp_secret_envvar: &str) -> Result<String> {
  let secret = std::env::var(totp_secret_envvar)?;
  let auth = GoogleAuthenticator::new();
  auth.get_code(&secret, 0).map_err(|e| anyhow!(e))
}

pub fn generate_nonce() -> String {
  let now = Utc::now();
  let seconds: i64 = now.timestamp();
  let nanoseconds: i64 = now.nanosecond() as i64;
  ((seconds * 1000) + (nanoseconds / 1000 / 1000)).to_string()
}

pub fn url_encode_from_hashmap(data: &HashMap<&str, String>) -> String {
  if data.is_empty() {
    return String::new();
  }
  let mut url_encode = String::new();
  for (name, arg) in data {
    url_encode += &format!("{}={}&", name, arg);
  }
  url_encode.pop(); // trim & we don't need it
  url_encode
}

pub fn get_signature(urlpath: &str, postdata: &str, nonce: &str) -> Result<String> {
  let api_secret = std::env::var("OMA_API_SECRET").expect("OMA_API_SECRET to get set");
  let encoded = format!("{}{}", nonce, postdata);

  let mut sha256 = Sha256::default();
  sha256.update(&encoded.as_bytes());
  let result = sha256.finalize();

  let mut message = urlpath.as_bytes().to_vec();
  for element in result {
    message.push(element);
  }

  let hmac_key = BASE64.decode(api_secret.as_bytes())?;
  let mut mac = HmacSha512::new_from_slice(&hmac_key[..])?;
  mac.update(&message);

  Ok(BASE64.encode(&mac.finalize().into_bytes()))
}
