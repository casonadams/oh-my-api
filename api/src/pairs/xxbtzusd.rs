use serde::Deserialize;

#[derive(Debug, Default, Clone, Deserialize)]
pub struct Xxbtzusd {
  pub a: Vec<String>,
  pub b: Vec<String>,
  pub c: Vec<String>,
  pub v: Vec<String>,
  pub p: Vec<String>,
  pub t: Vec<i32>,
  pub l: Vec<String>,
  pub h: Vec<String>,
  pub o: String,
}
