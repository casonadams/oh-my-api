use crate::auth::auth_utils;
use anyhow::Result;
use async_trait::async_trait;
use cucumber::WorldInit;
use serde::Deserialize;
use std::collections::HashMap;
use std::convert::Infallible;

#[derive(Debug, Default, Deserialize, Clone, WorldInit)]
pub struct OpenOrdersResponse {
  pub totp_secret_envvar: Option<String>,
  pub api_key_envvar: Option<String>,
  pub error: Vec<String>,
  pub result: Option<OpenOrdersResult>,
}

#[derive(Debug, Default, Clone, Deserialize)]
pub struct OpenOrdersResult {}

#[async_trait(?Send)]
impl cucumber::World for OpenOrdersResponse {
  type Error = Infallible;
  async fn new() -> Result<Self, Self::Error> {
    Ok(Self::default())
  }
}

impl OpenOrdersResponse {
  pub async fn get(&self, urlpath: String, use_otp: bool) -> Result<OpenOrdersResponse> {
    let base = std::env::var("OMA_API_BASE_URL").expect("OMA_API_BASE_URL to be set");
    let api_key = std::env::var(&self.api_key_envvar.as_ref().unwrap())
      .unwrap_or_else(|_| panic!("{} to be set", &self.api_key_envvar.as_ref().unwrap()));
    let url = format!("{}{}", base, urlpath);

    // setup signature generation
    let nonce = auth_utils::generate_nonce();
    let mut data: HashMap<&str, String> = HashMap::new();
    data.insert("nonce", nonce.to_string());
    if use_otp {
      let otp = auth_utils::generate_otp_code(self.totp_secret_envvar.as_ref().unwrap())?;
      data.insert("otp", otp);
    }
    let postdata = auth_utils::url_encode_from_hashmap(&data);

    let api_signature = auth_utils::get_signature(&urlpath, &postdata, &nonce).expect("message");

    let client = reqwest::Client::new();
    let response = client
      .post(url)
      .header("API-Key", &api_key)
      .header("API-Sign", &api_signature)
      .body(postdata)
      .send()
      .await?
      .json::<OpenOrdersResponse>()
      .await?;

    Ok(response)
  }
}
