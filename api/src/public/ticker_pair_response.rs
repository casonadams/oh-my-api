use crate::pairs::Xxbtzusd;
use anyhow::{anyhow, Result};
use async_trait::async_trait;
use cucumber::WorldInit;
use serde::Deserialize;
use std::convert::Infallible;

#[derive(Debug, Default, Deserialize, Clone, WorldInit)]
pub struct TickerPairResponse {
  pub error: Vec<String>,
  pub result: TickerPairResult,
}

#[derive(Debug, Default, Clone, Deserialize)]
pub struct TickerPairResult {
  #[serde(alias = "XXBTZUSD")]
  pub xxbtzusd: Xxbtzusd,
}

#[async_trait(?Send)]
impl cucumber::World for TickerPairResponse {
  type Error = Infallible;
  async fn new() -> Result<Self, Self::Error> {
    Ok(Self::default())
  }
}

impl TickerPairResponse {
  pub async fn get(public_ticket_pair_uri: String, arg: String) -> Result<TickerPairResponse> {
    let base = std::env::var("OMA_API_BASE_URL");
    let url = format!(
      "{}{}{}",
      base.as_ref().expect("OMA_API_BASE_URL must be set"),
      public_ticket_pair_uri,
      arg
    );

    match arg.as_str() {
      "XBTUSD" => reqwest::get(&url)
        .await
        .expect("a response")
        .json::<TickerPairResponse>()
        .await
        .map_err(|e| anyhow!(e)),
      _ => Err(anyhow!("EQuery:Unknown asset pair: {}", arg)),
    }
  }
}
