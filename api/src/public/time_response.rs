use anyhow::{anyhow, Result};
use async_trait::async_trait;
use cucumber::WorldInit;
use serde::Deserialize;
use std::convert::Infallible;

#[derive(Debug, Default, Deserialize, Clone, WorldInit)]
pub struct TimeResponse {
  pub error: Vec<String>,
  pub result: TimeResult,
}

#[derive(Debug, Default, Clone, Deserialize)]
pub struct TimeResult {
  pub unixtime: i64,
  pub rfc1123: String,
}

#[async_trait(?Send)]
impl cucumber::World for TimeResponse {
  type Error = Infallible;
  async fn new() -> Result<Self, Self::Error> {
    Ok(Self::default())
  }
}

impl TimeResponse {
  pub async fn get(public_time_uri: String) -> Result<TimeResponse> {
    let base = std::env::var("OMA_API_BASE_URL");
    let url = format!(
      "{}{}",
      base.as_ref().expect("OMA_API_BASE_URL must be set"),
      public_time_uri
    );

    reqwest::get(&url)
      .await
      .expect("a response")
      .json::<TimeResponse>()
      .await
      .map_err(|e| anyhow!(e))
  }
}
